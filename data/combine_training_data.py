#!/usr/bin/env python
#
# Only used to create the training_data.csv file from multiple data sources
#

import numpy as np
import pandas as pd


def read_training_csv(filename, annotation_col=None):
    if filename.endswith('.xlsx'):
        df = pd.read_excel(filename)
    else:
        df = pd.read_csv(filename)
    
    df.filename = filename
    df._metadata.append('filename')

    # Strip column names
    df = df.rename(columns=lambda x: x.strip())
    
    if 'm/z' in df.columns and 'mz' not in df.columns:
        df.rename(columns={'m/z': 'mz'}, inplace=True)
    
    if annotation_col is not None:
        df.rename(columns={annotation_col: 'annotation'}, inplace=True)
    
    if 'adduct' in df.columns:
        df.adduct = df.adduct.str.strip()
    
    # Validate data
    assert(df.CCS.dtype == np.float)
    assert(df.RT.dtype == np.float)
    assert(df.mz.dtype == np.float)
    
    print('Loaded %s with %d data points' % (filename, len(df)))
    
    return df

def generate_im_training_data():
    # Read all positive mode data
    df = read_training_csv('raw/IM_inputforClassification_POSITIVE_2.csv')

    # Remove severe outlier
    df = df[~((df.adduct == '[M+NH4]+') & (df.mz > 950) & (df.CCS < 260))]

    # Fix adduct for SM
    df.loc[df['class'] == 'SM', 'adduct'] = '[M+H]+'

    # Read old M+Na files
    # df2 = read_training_csv('raw/TG_Na_training.csv', 'LipidBlast_008_NaOnly_table')
    # df2['class'] = 'TG'
    # df2['annotation'] = 'TG'+ df2.annotation
    # df = df.append(df2)

    # Read old M+NH4 files and fix adduct type
    # df2 = read_training_csv('raw/TGs_NH4_training.csv', 'LipidBlast_008_NaOnly_table')
    # df2['class'] = 'TG'
    # df2['annotation'] = 'TG'+ df2.annotation
    # df2['adduct'] = '[M+NH4]+'
    # df = df.append(df2)

    # Read Master list and format
    df2 = read_training_csv('raw/Master file.xlsx')
    df2['adduct'] = df2.Match1.str.split('; ').str[1]
    df2['annotation'] = df2.Match1.str.split('; ').str[0]
    df2['class'] = df2.annotation.str.split(' ').str[0]
    df2['Cnum'] = df2.annotation.str.split(' ').str[1].str.split(':').str[0]
    df2['Snum'] = df2.annotation.str.split(':').str[1]
    df2 = df2.drop(['SD', 'Abundance', 'Hits Found', 'Match1'], 1)
    df = df.append(df2)

    # Drop unused lipid classes
    df = df[df['class'].isin(['TG', 'DG', 'PC', 'SM'])]

    # Reorder columns
    df = df[['mz', 'RT', 'CCS', 'annotation', 'adduct', 'class', 'Cnum', 'Snum']]

    print()
    print(df['class'].value_counts())
    print()
    print(df['adduct'].value_counts())

    # Export data frame
    df.to_csv('im_training_data.csv', index=False, float_format='%0.4f')


def generate_im_unknown_data():
    df = pd.read_csv('raw/All_IM_features_POSITIVE.csv', skiprows=4)
    df = df.rename({'m/z': 'mz'}, axis=1)
    df = df[['ID', 'mz', 'RT', 'CCS']]

    df.to_csv('im_unknown_data.csv', index=False, float_format='%0.4f')


def generate_qtof_training_data():
    df = pd.read_excel('raw/mzRT_MSMS_Bovine_results_MSDIAL.xlsx', skiprows=3)
    df = df.rename({'Average Mz': 'mz', 'Average Rt(min)': 'RT', 'Metabolite name': 'annotation', 'Adduct ion name': 'adduct'}, axis=1)

    # Remove unknowns
    df = df[df.annotation != 'Unknown']
    # df = df[df.annotation.str.split().str[0].isin(['TG', 'DG', 'PC', 'SM'])]

    # Masks
    sim_match = df['Fragment presence %'] > 0
    good_sim_match = (df['Dot product'] > 400) & (df['Reverse dot product'] > 400)
    wo_msms = df.annotation.str.contains('w/o MS2')

    # Take subset of columns
    df = df[['mz', 'RT', 'annotation', 'adduct']]

    # Normalize annotations
    def normalize_annotation(s):
        s = s.split()
        s[1] = s[1].lstrip('(').lstrip('d').rstrip(')')

        cnum, snum = 0, 0

        if '/' in s[1]:
            for x in s[1].split('/'):
                x = x.split(':')
                cnum += int(x[0])
                snum += int(x[1])

            s[1] = '%d:%d' % (cnum, snum)

        return '%s %s' % (s[0], s[1])

    df_mzrt_lib = df[~sim_match & ~wo_msms].copy()
    df_mzrt_lib = df_mzrt_lib[df_mzrt_lib.annotation.str.split().str[0].isin(['TG', 'DG', 'PC', 'SM'])]
    df_mzrt_lib.adduct = '['+ df_mzrt_lib.annotation.str.split('[').str[1].str.split(']').str[0] + ']+'
    df_mzrt_lib.annotation = df_mzrt_lib.annotation.str.split('[').str[0].apply(normalize_annotation)    

    df_sim = df[good_sim_match].copy()
    df_sim.adduct = df_sim.annotation.str.split(';').str[-1].str.strip()
    df_sim.annotation = df_sim.annotation.str.split(';').str[0].apply(normalize_annotation)

    df_wo_msms = df[wo_msms].copy()
    df_wo_msms.annotation = df[wo_msms].annotation.str.split(':', 1).str[1].str.split(';').str[0].str.strip()
    df_wo_msms = df_wo_msms[df_wo_msms.annotation.str.split().str[0].isin(['TG', 'DG', 'PC', 'SM'])]

    # Export high confidence annotations
    df = df_mzrt_lib.append(df_sim)
    df['class'] = df.annotation.str.split().str[0]
    df['Cnum'] = df.annotation.str.split().str[1].str.split(':').str[0]
    df['Snum'] = df.annotation.str.split(':').str[1]

    print()
    print(df['class'].value_counts())
    print()
    print(df['adduct'].value_counts())

    df.to_csv('qtof_training_data.csv', index=False, float_format='%0.4f')

    # Export wo_msms annotations included
    df = df_mzrt_lib.append(df_sim).append(df_wo_msms)
    df['class'] = df.annotation.str.split().str[0]
    df['Cnum'] = df.annotation.str.split().str[1].str.split(':').str[0]
    df['Snum'] = df.annotation.str.split(':').str[1]

    print()
    print(df['class'].value_counts())
    print()
    print(df['adduct'].value_counts())

    df.to_csv('qtof_training_data_full.csv', index=False, float_format='%0.4f')


if __name__ == '__main__':
    generate_im_training_data()
    generate_im_unknown_data()
    generate_qtof_training_data()
