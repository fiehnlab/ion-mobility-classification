#/usr/bin/env python
#
# Uses the results of the 
#

import molmass
import numpy as np
import pandas as pd
import scipy.interpolate as interp
import sys
import tqdm


# List of adducts to include in search
ADDUCT_MAP = {
    '[M+H]+': 1.007276,
    '[M+Na]+': 22.98218,
    '[M+NH4]+': 18.033823,
    '[M+K]+': 38.963158
}


#
# Mass Calculation
#

def generate_formula(compound_class, c, s):
    """Generate a standard molecular formula for the given lipid"""

    if compound_class == 'TG':
        return 'C%dH%dO6' % (3 + c, 2 * (c - s + 1))

    elif compound_class == 'DG':
        return 'C%dH%dO5' % (3 + c, 2 * (c - s + 2))

    elif compound_class == 'PC':
        return 'C%dH%dNO8P' % (8 + c, 2 * (c - s + 8))

    elif compound_class == 'SM':
        return 'C%dH%dN2O6P' % (5 + c, 2 * (c - s) + 13)

    else:
        return None

def formula_to_monoisotopic_mass(formula):
    """Generate the monoisotopic mass from the given molecular formula"""

    if formula is not None:
        return molmass.Formula(formula).isotope.mass
    else:
        return -1


prediction_memo = {}

def calculate_monoisotopic_mass(compound_class, c, s):
    """
    Calculate the monoisotopic mass for the given lipid, using memoization
    to avoid repeated calculations
    """

    if (compound_class, c, s) not in prediction_memo:
        prediction_memo[(compound_class, c, s)] = formula_to_monoisotopic_mass(generate_formula(compound_class, c, s))

    return prediction_memo[(compound_class, c, s)]


#
# Carbon number estimation
#

def jitter(mz, n=10):
    if type(mz) == float:
        return mz + (n * mz / 1.0e6) * (2 * np.random.random_sample() - 1)
    else:
        return mz + (n * mz / 1.0e6) * (2 * np.random.random_sample(size=len(mz)) - 1)


class InterpolationRegression:

    def __init__(self, df, jitter_value=None):
        self.mz = {}
        self.left = {}
        self.right = {}
        self.interp = {}

        for c in set(df['class']):
            x = df[df['class'] == c]

            self.mz[c] = jitter(x.mz, jitter_value) if jitter_value is not None else x.mz

            # Interpolation
            self.interp[c] = interp.interp1d(self.mz[c], x.Cnum)
            
            # Linear regressions for extrapolation
            slope = np.polyfit(self.mz[c], x.Cnum, 1)[0]

            self.left[c] = np.poly1d([slope, min(x.Cnum) - slope * min(self.mz[c])])
            self.right[c] = np.poly1d([slope, max(x.Cnum) - slope * max(self.mz[c])])

    def predict(self, compound_class, mz):
        if compound_class not in self.mz:
            return -1
        elif mz < min(self.mz[compound_class]):
            return self.left[compound_class](mz)
        elif mz > max(self.mz[compound_class]):
            return self.right[compound_class](mz)
        else:
            return float(self.interp[compound_class](mz))


#
# Predictions
#


def predict_saturation(compound_class, c, mz):
    """
    Predict saturation number and adduct from a classified compound class,
    carbon number and measured m/z
    """

    min_mass_diff = None
    min_saturation_number = None
    min_adduct = None

    for s in range(c // 3):
        for adduct, adduct_mass in ADDUCT_MAP.items():
            # Predict molecular formula and mass
            mass = calculate_monoisotopic_mass(compound_class, c, s)
            mz_diff = mz - mass - adduct_mass

            if min_mass_diff is None or abs(mz_diff) < abs(min_mass_diff):
                min_mass_diff = mz - mass - adduct_mass
                min_saturation_number = s
                min_adduct = adduct

    return min_saturation_number, min_adduct, min_mass_diff


def predict_all(compound_class, mz, cnum_estimator, tol=0.01):
    """
    Predict carbon number, saturation number and adduct from a
    classified compound class and measured m/z
    """

    min_mass_diff = None
    min_carbon_number = None
    min_saturation_number = None
    min_adduct = None

    # Estimate carbon number from compound class and m/z
    estimated_c = int(round(cnum_estimator.predict(compound_class, mz)))

    # Search from the estimated value, increasing until a maximal search window
    MAX_SEARCH_WINDOW = 5

    for dc in sorted(range(-MAX_SEARCH_WINDOW, MAX_SEARCH_WINDOW + 1), key=abs):
        c = estimated_c + dc

        saturation_number, adduct, mass_diff = predict_saturation(compound_class, c, mz)

        if min_mass_diff is None or (mass_diff is not None and abs(mass_diff) < abs(min_mass_diff)):
            min_mass_diff = mass_diff
            min_carbon_number = c
            min_saturation_number = saturation_number
            min_adduct = adduct

        if dc > 0 and min_mass_diff is not None and abs(min_mass_diff) < tol:
            return min_carbon_number, min_saturation_number, min_adduct, min_mass_diff

    # No match was found
    return min_carbon_number, min_saturation_number, min_adduct, min_mass_diff


if __name__ == '__main__':
    # Test prediction of only saturation number
    total_saturation = 0
    total_adduct = 0
    total = 0

    for i in tqdm.tqdm(range(10000)):
        df = pd.read_csv('data/im_training_data.csv')
        df.mz = df.mz.apply(lambda x: jitter(x, 10))

        results = df.apply(lambda x: predict_saturation(x['class'], x['Cnum'], x['mz']), axis=1)
        saturation_number, adduct, mz_diff = zip(*results)

        total_saturation += sum(np.array(saturation_number) == df.Snum)
        total_adduct += sum(np.array(adduct) == df.adduct)
        total += len(df)

    print('Lipid Class + Carbon Number -> Saturation Number + Adduct:')
    print('\tSaturation number prediction:', total_saturation / total)
    print('\tAdduct prediction:', total_adduct / total)
    print()

    


    # Test prediction of carbon number and saturation number
    def predict(x, verbose=False):
        results = predict_all(x['class'], x['mz'], cnum_estimator)

        if verbose:
            print('({}, {}, {}, {}) -> '.format(x['class'], x.mz, x.RT, x.CCS), end='')
            print('({}, {}, {}): '.format(*results), end='')

            if results[0] == x.Cnum and results[1] == x.Snum and results[2] == x.adduct:
                print('Success!')
            else:
                print('Failed! ({}, {}, {})'.format(x.Cnum, x.Snum, x.adduct))

        return results

    # Get prediction results
    total_carbon = 0
    total_saturation = 0
    total_adduct = 0
    total = 0

    for i in tqdm.tqdm(range(10000)):
        df = pd.read_csv('data/im_training_data.csv')
        cnum_estimator = InterpolationRegression(df)
        df.mz = df.mz.apply(lambda x: jitter(x, 10))

        results = df.apply(predict, axis=1)
        carbon_number, saturation_number, adduct, mz_diff = zip(*results)

        total_carbon += sum(np.array(carbon_number) == df.Cnum)
        total_saturation += sum(np.array(saturation_number) == df.Snum)
        total_adduct += sum(np.array(adduct) == df.adduct)
        total += len(df)

    print('Lipid Class -> Carbon Number + Saturation Number + Adduct:')
    print('\tCarbon number prediction:', total_carbon / total)
    print('\tSaturation number prediction:', total_saturation / total)
    print('\tAdduct prediction:', total_adduct / total)
