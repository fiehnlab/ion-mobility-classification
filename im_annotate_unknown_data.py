#!/usr/bin/env python

from __future__ import division, print_function
import pandas as pd

from im_classification import *
from im_lipid_prediction import *


MATCH_TOL = 0.01


if __name__ == '__main__':
    # Load training and test data sets
    training_data = pd.read_csv('data/im_training_data.csv')
    unknown_data = pd.read_csv('data/im_unknown_data.csv')

    # Build carbon number estimator
    cnum_estimator = InterpolationRegression(training_data)

    i = 0

    for k_X, X in X_train.items():
        for k_y, y in y_train.items():
            print('Running Classifier #%d: %s -> %s...' % (i, k_X, k_y))

            y_factors, y_labels = y(training_data)

            pipeline = classifiers[i]
            pipeline.fit(X(training_data), y_factors)

            y_test = pipeline.predict(X(unknown_data))

            class_id = 'Classifier #%d' % (i + 1)
            unknown_data[class_id] = list(map(y_labels.__getitem__, y_test))

            if k_y == 'Lipid Class + Carbon Number':
                for j, row in unknown_data.iterrows():
                    results = predict_saturation(row[class_id].split()[0], int(row[class_id].split()[1]), row.mz)

                    # Check that a match exists
                    if all(x is not None for x in results):
                        saturation_number, adduct, mz_diff = results

                        # Check that the mass differnece is reasonable
                        if abs(mz_diff) < MATCH_TOL:
                            unknown_data.loc[j, class_id] += ':%d %s' % (saturation_number, adduct)
                        else:
                            unknown_data.loc[j, class_id] = ''
                            # print('\tInvalid mass match for (%.4f, %.2f, %.2f) with dm/z = %.3f' % (row.mz, row.RT, row.CCS, mz_diff))
                    else:
                        unknown_data.loc[j, class_id] = ''
                        # print('\tNo prediction available for (%.4f, %.2f, %.2f)' % (row.mz, row.RT, row.CCS))

            else:
                for j, row in unknown_data.iterrows():
                    results = predict_all(row[class_id], row.mz, cnum_estimator)

                    # Check that a match exists
                    if all(x is not None for x in results):
                        carbon_number, saturation_number, adduct, mz_diff = results

                        # Check that the mass differnece is reasonable
                        if abs(mz_diff) < MATCH_TOL:
                            unknown_data.loc[j, class_id] += ' %d:%d %s' % (carbon_number, saturation_number, adduct)
                        else:
                            unknown_data.loc[j, class_id] = ''
                            # print('\tInvalid mass match for (%.4f, %.2f, %.2f) with dm/z = %.3f' % (row.mz, row.RT, row.CCS, mz_diff))
                    else:
                        unknown_data.loc[j, class_id] = ''
                        # print('\tNo prediction available for (%.4f, %.2f, %.2f)' % (row.mz, row.RT, row.CCS))


            i += 1

    unknown_data.to_csv('im_unknown_data_annotated.csv')
    print()


    # Show summary of annotated features
    for i in range(6):
        col = 'Classifier #%d' % (i + 1)
        annotated = unknown_data[unknown_data[col] != '']

        known_filter = annotated.apply(lambda x: not any((abs(x.mz - training_data.mz) < 0.01) & (abs(x.RT - training_data.RT) < 0.01) & (abs(x.CCS - training_data.CCS) < 0.01)), axis=1)

        print(len(annotated), 'total and', len(annotated[known_filter]), 'unknown lipids annotated using', col)
        print(annotated[known_filter][col].str.split(' ').str[0].value_counts())
        print()
