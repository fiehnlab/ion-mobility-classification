# Ion Mobility Lipidomics Classification

This repository contains the source code and data used in the project entitled "Increasing compound identification rates in untargeted lipidomics research with liquid chromatography drift time-ion mobility mass spectrometry."  

This research was performed at the [Fiehn Research Lab](http://fiehnlab.ucdavis.edu/).

## Usage

### Classification

`im_classification.py` contains the training sets and classifiers used for identifying the compound class and carbon number for lipids observed using ion mobility instruments.

The script runs the predefined classifications for each training set and label and provides a cross validation score.

```
Input               Label                                     CV Score
----------------------------------------------------------------------
m/z, RT             Lipid Class + Carbon Number                0.84372
m/z, RT             Lipid Class                                0.93512
----------------------------------------------------------------------
m/z, CCS            Lipid Class + Carbon Number                0.90093
m/z, CCS            Lipid Class                                0.94977
----------------------------------------------------------------------
m/z, RT, CCS        Lipid Class + Carbon Number                0.92488
m/z, RT, CCS        Lipid Class                                0.95558
----------------------------------------------------------------------
```

### Carbon Number, Saturation Number and Adduct Prediction

`im_lipid_prediction.py` accepts the results of the either the Lipid Class or the Lipid Class + Carbon Number classification as well as the observed m/z and predicts the remaining lipid properties.  

1) The first prediction calculates the best saturation number and adduct match given a lipid's class, carbon number and observed m/z.  This applies to all of the lipid classes represented in the training set: TG, DG, PC and SM.  Running on the training set data yields 100% predictions.

2) The second prediction removed the reliance on carbon number and tries to estimate it instead using an interpolation method.  It then applies the above saturation number and adduct prediction for nearby carbon number values.  Using the training data in the estimator, we also get a 100% success rate.

To test it more robustly, we applying a random jitter with maximum 50 ppm magnitude.  This ensures that we no longer have perfect m/z matches and mimics the how new incoming data will react.  Over 1000 iterations using random jitters, we have a success rate of 97.34% for predicting the full lipid information.  For example, an input of (TG, 916.8314) results in a correct prediction of TG 55:3 [M+NH4]+.  Combining this with the accuracy of the Lipid Class classifier yields a total accuracy of 92.84%.

```
Lipid Class + Carbon Number -> Saturation Number + Adduct:
	Saturation number prediction: 1.0
	Adduct prediction: 1.0


Lipid Class -> Carbon Number + Saturation Number + Adduct:
	Carbon number prediction: 0.9954125874125874
	Saturation number prediction: 0.9954125874125874
	Adduct prediction: 0.9999347319347319
```

### Annotation of unknown data

`im_annotate_unknown_data.py` accepts a csv data table containing m/z, RT and CCS values and appends columns containing full lipid annotation predictions using each of the six defined classifiers.  It will by default use the existing training and unknown data files.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.