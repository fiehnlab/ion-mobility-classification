#/usr/bin/env python
#
# This approach to classifiction utilizes TPOT to optimize and combine a large
# set of machine learning classifiers with various pre-processing steps and
# transformations (e.g., PCA and LDA).  The optimial classification for each
# dataset is then chosen.
#

from __future__ import division, print_function
import itertools
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler

from im_classification import *



if __name__ == '__main__':
    # Load training data
    df_im = pd.read_csv('data/im_training_data.csv')
    df_qtof = pd.read_csv('data/qtof_training_data.csv')


    # (m/z, RT) -> lipid class
    X_train_im = df_im[['mz', 'RT']]
    y_train_im = pd.factorize(df_im.annotation.str.split(' ').str[0])[0]

    X_train_qtof = df_qtof[['mz', 'RT']]
    y_train_qtof = pd.factorize(df_qtof.annotation.str.split(' ').str[0])[0]


    # Test IM classifier using QTOF data
    pipeline = make_pipeline(
        MinMaxScaler(),
        KNeighborsClassifier(n_neighbors=7, p=2, weights='distance')
    )
    pipeline.fit(X_train_im, y_train_im)

    accuracy = sum(pipeline.predict(X_train_qtof) == y_train_qtof) / len(y_train_qtof)
    print('IM Classifier (KNN) trained with IM data predicting on QTOF data: %.4f' % accuracy)


    # Test QTOF classifier using IM data
    pipeline.fit(X_train_qtof, y_train_qtof)

    accuracy = sum(pipeline.predict(X_train_im) == y_train_im) / len(y_train_im)
    print('IM Classifier (KNN) trained with QTOF data predicting on IM data: %.4f' % accuracy)


    pipeline = make_pipeline(
        PCA(iterated_power=3, svd_solver='randomized'),
        GaussianNB()
    )
    pipeline.fit(X_train_qtof, y_train_qtof)

    accuracy = sum(pipeline.predict(X_train_im) == y_train_im) / len(y_train_im)
    print('QTOF Classifier (GaussianNB) trained with QTOF data predicting on IM data: %.4f' % accuracy)
