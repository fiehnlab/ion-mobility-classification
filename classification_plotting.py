#!/usr/bin/env python

from __future__ import division, print_function
import matplotlib
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as mplot3d
import numpy as np
import pandas as pd


def plot_mz_ccs(df):
    # Group data by adduct
    grouped = df.groupby('adduct')
    groups = {k: grouped.get_group(k) for k in grouped.groups}

    colors = ['r', 'b', 'g', 'k']
    #matplotlib.rcParams['figure.figsize']=(12,8)

    # CCS vs m/z
    fig, ax = plt.subplots()

    for i, (key, group) in enumerate(groups.items()):
        plt.scatter(group.mz, group.CCS, marker='.', label=key, cmap=plt.cm.Set1)

    plt.xlabel('m/z')
    plt.ylabel('CCS')
    plt.legend(loc = 'best')
    fig.tight_layout()

    plt.savefig('plots/classification_ccs_vs_mz.png', dpi=600)
    plt.savefig('plots/classification_ccs_vs_mz.pdf')

    # CCS vs m/z and RT
    fig = plt.figure()
    ax = mplot3d.Axes3D(fig, elev=-150, azim=190)

    for i, (key, group) in enumerate(groups.items()):
        ax.scatter(group.mz, group.RT, group.CCS, label=key, cmap=plt.cm.Set1, s=20)
        
    ax.set_xlabel('m/z')
    ax.set_ylabel('RT')
    ax.set_zlabel('CCS')
    plt.legend(loc = 'best')

    plt.savefig('plots/classification_ccs_vs_mz_3d.png', dpi=600)
    plt.savefig('plots/classification_ccs_vs_mz_3d.pdf')


def plot_cnum_interpolation(df):
    plt.style.use('seaborn-paper')

    df = df[df['class'] == 'TG']

    fig = plt.figure()
    #plt.title('Carbon Number Interpolation for TGs')

    for c in sorted(set(df.Cnum)):
        x = df[df.Cnum == c]
        plt.scatter(x.mz, x.Cnum, label=c)

    t = np.linspace(min(df.mz - 25), max(df.mz + 25), 1000)

    df = df.drop_duplicates(['mz', 'class'])
    df = df.sort_values(by='mz')

    from im_lipid_prediction import InterpolationRegression
    fit = InterpolationRegression(df).predict

    plt.plot(t, [fit('TG', q) for q in t], 'b-', label=None)

    plt.xlabel('m/z')
    plt.ylabel('Carbon Number')
    plt.xlim((min(t), max(t)))
    plt.legend(loc='best', prop={'size': 6})
    fig.tight_layout()

    plt.savefig('plots/carbon_number_interpolation.png', dpi=600)
    plt.savefig('plots/carbon_number_interpolation.pdf')
    plt.show(block=False)


if __name__ == '__main__':
    df = pd.read_csv('data/im_training_data.csv')

    plot_mz_ccs(df)
    plot_cnum_interpolation(df)
