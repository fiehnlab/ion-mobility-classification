#/usr/bin/env python
#
# This approach to classifiction utilizes TPOT to optimize and combine a large
# set of machine learning classifiers with various pre-processing steps and
# transformations (e.g., PCA and LDA).  The optimial classification for each
# dataset is then chosen.
#

from __future__ import division, print_function
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from tpot import TPOTClassifier


# Define the sets of training data and labels to utilize
X_train = {
    # Simulating QTOF input with just m/z and RT
    'm/z, RT': lambda df: df[['mz', 'RT']],
    
    # Simplest data input of just m/z and CCS
    'm/z, CCS': lambda df: df[['mz', 'CCS']],

    # Including RT
    'm/z, RT, CCS': lambda df: df[['mz', 'RT', 'CCS']]
}

y_train = {
    # Lipid class + carbon number
    'Lipid Class + Carbon Number': lambda df: pd.factorize(df.annotation.str.split(':').str[0]),

    # Lipid class
    'Lipid Class': lambda df: pd.factorize(df.annotation.str.split(' ').str[0]),
}

# Chosen classifier pipelines to use for each set of training data.
# These were selected by running TPOT with different configurations
# over different numbers of generations, choosing based on internal
# CV score and simplicity
classifiers = [
    # (m/z, RT) -> lipid class + carbon number
    make_pipeline(
        StandardScaler(),
        KNeighborsClassifier(n_neighbors=3, p=1, weights='distance')
    ),

    # (m/z, RT) -> lipid class
    KNeighborsClassifier(n_neighbors=7, p=2, weights='distance'),


    # (m/z, CCS) -> lipid class + carbon number
    KNeighborsClassifier(n_neighbors=3, p=1, weights='distance'),

    # (m/z, CCS) -> lipid class
    make_pipeline(
        StandardScaler(),
        KNeighborsClassifier(n_neighbors=3, p=2, weights='distance')
    ),


    # (m/z, RT, CCS) -> lipid class + carbon number
    KNeighborsClassifier(n_neighbors=1, p=1, weights='distance'),

    # (m/z, RT, CCS) -> lipid class
    make_pipeline(
        StandardScaler(),
        KNeighborsClassifier(n_neighbors=13, p=1, weights='distance')
    )
]



def auto_train(X, y, generations=5, verbosity=2):
    """Simple use of TPOT to """

    pipeline_optimizer = TPOTClassifier(generations=generations, config_dict='TPOT light', verbosity=verbosity)
    pipeline_optimizer.fit(X, y)
    return pipeline_optimizer


def test_pipeline(pipeline, X, y, n=1000, test_size=0.2):
    """"""

    correct, count = 0, 0

    for i in range(n):
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=1.0 - test_size, test_size=test_size, random_state=i)
        pipeline.fit(X_train, y_train)
        
        correct += sum(pipeline.predict(X_test) == y_test)
        count += len(y_test)

    return correct, count, correct / count

def validate_pipeline(pipeline, X, y):
    """Test a trained pipeline against a validation set"""
    return sum(pipeline.predict(X) == y) / len(y)



if __name__ == '__main__':
    # Load combined ion mobility training data
    df = pd.read_csv('data/im_training_data.csv')


    # Run classifications for all combinations of training data and labels
    print('{:<20}{:<35}{:>15}'.format('Input', 'Label', 'CV Score'))
    print('-' * 70)
    i = 0

    for k_X, X in X_train.items():
        for k_y, y in y_train.items():
            _, _, ratio = test_pipeline(classifiers[i], X(df), y(df)[0], n=50)

            print('{:<20}{:<35}{:>15}'.format(k_X, k_y, '{:.5f}'.format(ratio)))
            i += 1

        print('-' * 70)
